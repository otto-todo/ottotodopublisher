﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.EventGrid;
using Microsoft.Azure.EventGrid.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Otto.Todo.Publisher.EventModel;

namespace Otto.Todo.Publisher
{
    
    public class EventPublisher : ControllerBase
    {
        private readonly IConfiguration _configuration;
        string topicEndpoint;
        string topicKey;
        public EventPublisher(IConfiguration configuration)
        {
            _configuration = configuration;
           
        }
        public async Task<IActionResult> PublishAsync(string eventType, string message)
        {
            // TODO: Enter values for <topic-name> and<region>.You can find this topic endpoint value
            // in the "Overview" section in the "Event Grid Topics" blade in Azure Portal.
            topicEndpoint = Environment.GetEnvironmentVariable("TOPIC_ENDPOINT");
            //topicEndpoint = _configuration.GetSection("TopicEndpoint").Value;


            // TODO: Enter value for <topic-key>. You can find this in the "Access Keys" section in the
            // "Event Grid Topics" blade in Azure Portal.
            topicKey = Environment.GetEnvironmentVariable("TOPIC_KEY");
            //topicKey = _configuration.GetSection("TopicKey").Value;

            string topicHostname = new Uri(topicEndpoint).Host;
            TopicCredentials topicCredentials = new TopicCredentials(topicKey);
            EventGridClient client = new EventGridClient(topicCredentials);

            client.PublishEventsAsync(topicHostname, GetEventsList(eventType, message)).GetAwaiter().GetResult();
            Console.Write("Published events to Event Grid topic.");
            //Console.ReadLine();
            return Ok();
        }

        IList<EventGridEvent> GetEventsList(string eventType, string message)
        {
            List<EventGridEvent> eventsList = new List<EventGridEvent>();

            eventsList.Add(new EventGridEvent()
            {
                Id = Guid.NewGuid().ToString(),
                EventType = eventType,
                Data = message,
                EventTime = DateTime.Now,
                Subject = "Azure Eventgrid Publisher",
                DataVersion = "1.0"
            });

            return eventsList;
        }
    }
}
